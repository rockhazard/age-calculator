# README #

Age Calculator

### What is this repository for? ###

* Calculates age in years and the weekday of birth.

### How do I get set up? ###

* Have Python 3 installed.
* Set age.py to executable.
* Type "age.py --help"

### Author Contact ###

* rockhazardz@gmail.com