#!/usr/bin/env python3
"""
===============================================================================
FILE: age.py

USAGE: age.py [-h]

DESCRIPTION: returns weekday of birth and years of age when given a birth date
in the form of YYYY/M/D

REQUIREMENTS: Python 3
AUTHOR: rockhazardz@gmail.com
VERSION: 1.0a "Rainman"
CREATED: 06/24/2016
LICENSE: MIT
===============================================================================
"""
import argparse
import sys
from datetime import date


def calculate_age(year, month, day):
    """calculate age from birth date"""
    today = date.today()
    birth_day = date(year, month, day)
    age = today - birth_day
    years = int(age.days / 365)
    day_names = ("Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
                 "Saturday", "Sunday")
    week_day = day_names[birth_day.weekday()]
    dates = {"birth_day": birth_day, "age": age, "years": years,
             "weekday": week_day}
    return dates


def main(argv):
    """
    COMMANDLINE OPTIONS
    """
    # someone decided it wasn't worth fixing a bug in Argeparse.
    # Argparse chokes if metavar returns a tuple on a positional argument.
    # Hence the short help weirdness in repeating "YEAR MONTH DAY".
    # http://psf.upfronthosting.co.za/roundup/tracker/issue14074
    parser = argparse.ArgumentParser(prog=sys.argv[0][2:], description=
        """%(prog)s reports years of age and weekday of birth.""")
    parser.add_argument('--version', help='print version info then exit',
        version='%(prog)s 1.0a "Rainman"', action='version')
    parser.add_argument('birthdate', type=int, help=
    """Prints the years of age and weekday of birth for
    given birth date. Takes three separate arguments for
    year, month, and day (e.g. 1990 10 20).""", nargs='+',
                        metavar='YEAR MONTH DAY')
    args = parser.parse_args()

    birth = calculate_age(year=args.birthdate[0], month=args.birthdate[1],
                          day=args.birthdate[2])
    print('You were born on a {} and are {} years of age.'.format(
        birth["weekday"], birth["years"]))


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
